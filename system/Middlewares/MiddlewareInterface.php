<?php

namespace System\Middlewares;

use Symfony\Component\HttpFoundation\Request;

interface MiddlewareInterface 
{
	public function canProceed(Request $request);
}