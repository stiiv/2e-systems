<?php

namespace System\Pages;

use Illuminate\Container\Container as App;
use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Exceptions\RepositoryException;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use System\Pages\HttpMethodNotAllowedException;
use App\Helpers\Filesystem;
use App\Helpers\Environment;
use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig_Extension_Debug;
use App\Helpers\Html\Form;

use App\Auth\User as AuthUser;

abstract class Page implements PageInterface
{
	protected $app;

	protected $title;

	protected $request;

	protected $session;

	protected $repository;

	protected $templateEngine;

	protected $templateEngine_Filesystem;

	protected $templateEngine_Cache;

	protected $currUser;

	public function __construct(Request $request)
	{
		$this->app = new App;
		$this->request = $request;
		$this->templateEngine_Filesystem = implode(DIRECTORY_SEPARATOR, [BASE_DIR, 'app', 'Views']);
		$this->templateEngine_Cache = DIRECTORY_SEPARATOR.'template_cache';
		$this->setTemplatingEngine();
		$this->setRepository();

		$this->currUser = AuthUser::get();
	}

	public function getTitle() 
	{
		return $this->title;
	}

	public function loadView($filename, $vars = [])
	{
		echo $this->templateEngine->render(
			$filename,
			$vars
		);
	}

	abstract protected function repository();

	protected function setRepository()
	{
		$repository = $this->app->make($this->repository());
 
        if (!$repository instanceof RepositoryInterface)
            throw new RepositoryException(
            	"Class {$this->repository()} must be an instance of ".RepositoryInterface::class
            );
 
        $this->repository = $repository;
	}

	protected function setTemplatingEngine()
	{
		$loader = new Twig_Loader_Filesystem($this->templateEngine_Filesystem);
		$cacheFolder = $this->templateEngine_Filesystem.$this->templateEngine_Cache;

		$this->templateEngine = new Twig_Environment($loader, array(
			'debug' => true,
		    'cache' => $cacheFolder,
		));
		$this->templateEngine->addExtension(new Twig_Extension_Debug());

		if( getenv('CACHE_VIEWS') === 'false' && is_dir($cacheFolder) ) {
			Filesystem::delete_dir($cacheFolder);
		}

		// global vars
		$this->templateEngine->addGlobal('currUser', $this->currUser);
		$this->templateEngine->addGlobal('env', $this->app->make(Environment::class));
		$this->templateEngine->addGlobal('Form', $this->app->make(Form::class));
		$this->session = $this->request->hasSession() ? $this->request->getSession() : new Session;
		$this->templateEngine->addGlobal('session', $this->session);
	}

	protected function httpMethodAllowed($method = 'GET')
	{
		$method = strtoupper($method);
		if( $this->request->getMethod() !== $method ) {
			throw new HttpMethodNotAllowedException(
				sprintf('Only method "%s" allowed', $method)
			);
		}
	}

	protected function redirectTo($route)
	{
		return (new RedirectResponse($route))->send();
	}

	protected function flashSuccess($message)
	{
		$this->session->getFlashBag()->set('flash_success', $message);
	}

	protected function flashError($message)
	{
		$this->session->getFlashBag()->set('flash_error', $message);
	}
}