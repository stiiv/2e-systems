<?php 

namespace System\Pages;

use Exception;

class HttpMethodNotAllowedException extends Exception {}