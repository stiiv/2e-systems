<?php 

namespace System\Pages;

interface PageInterface
{
	public function getTitle();

	public function loadView($filename, $vars = []);
}