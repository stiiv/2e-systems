<?php 

namespace System\Router;

use Symfony\Component\HttpFoundation\Request;

class Router 
{
	protected static $routes = [];

	protected static $request;

	public static $actionSeparator = '@';

	public static $separator = '/';

	public static function setRequest(Request $request)
	{
		self::$request = $request;
	}

	public static function getRoutes() 
	{
		return self::$routes;
	}

	/**
	 * load in array of routes
	 * @param  array  $routes key = route; value = PageName.self::$actionSeparator.action
	 * @return void
	 */
	/*public static function load(array $routes)
	{
		self::$routes = $routes;
	}*/

	/**
	 * regex pattern for splitting params from route name
	 * @param  boolean $withSlashes - include route params separator or not
	 * @return string  (regex pattern)
	 */
	public static function paramsPattern($withSlashes = false)
	{
		$slashesIncluded = $withSlashes ? self::$separator : '';
		return '#'.$slashesIncluded.'\{.+\}#';
	}

	public static function routeExists($routeName)
	{
		return self::findRoute($routeName) !== null;
	}

	public static function findRoute($routeName)
	{
		$separator = self::$separator;

		if( $routeName === $separator )
			return $separator;

		$parts = explode($separator, $routeName);
		$segmeent = '';

		foreach ($parts as $part) {
			if( $part !== '' )
				$segmeent .= $separator.$part;

			if( array_key_exists($segmeent, self::$routes) )
				return $segmeent;
		}

		return null;
	}

	public static function findRouteParams($routeName)
	{
		$route = self::findRoute($routeName);
		$params = [];

		if($route === null)
			return $params;

		$route = trim( str_replace($route, '', $routeName), " \t\n\r\0\x0B/" );

		if( $route !== '' ) {
			$params = explode('/', $route);
		}

		return $params;
	}

	public static function add($route, $action, $middlewares = [])
	{
		/*if( self::routeExists($route) )
			throw new RouteAlreadyExistsExcepion("Route '{$route}' has already been defined!");*/

		$params = self::getArguments($route);
		$route = preg_replace(self::paramsPattern(true), '', $route);

		self::$routes[$route] = [
			'action' => $action, 
			'params' => $params,
			'middlewares' => $middlewares
		];
	}

	public static function resolve($route) 
	{
		if( !self::routeExists($route) )
			throw new RouteNotExistsExcepion("Route '{$route}' has not been defined!");

		$foundRoute = self::$routes[self::findRoute($route)];
		$parts = explode(self::$actionSeparator, $foundRoute['action'] );

		if( !isset($parts[0], $parts[1]) )
			return;

		// run middlwares if any before page load
		if( isset($foundRoute['middlewares']) 
			&& is_array($foundRoute['middlewares']) 
			&& count($foundRoute['middlewares'])
		) {
			foreach( $foundRoute['middlewares'] as $middleware ) {
				(new $middleware)->canProceed(self::$request);
			}
		}

		$obj = new $parts[0](self::$request);
		$method = $parts[1];

		return call_user_func_array([$obj, $method], self::findRouteParams($route));
	}

	public static function remove($route)
	{
		if( self::routeExists($route) )
			unset(self::$routes[$route]);
	}

	/**
	 * search method for arguments
	 * @param  string $action    name of the action/method
	 * @return array 
	 */
	public static function getArguments($route) 
	{
		$args = [];

		if( preg_match(self::paramsPattern(), $route, $matches) ) {
			$args = explode(self::$separator, $matches[0]);
		}

		return $args;
	}
}