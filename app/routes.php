<?php 

use System\Router\Router;

$pageNamespace = 'App\\Pages\\';
$middlewareNamespace = 'App\\Middlewares\\';

Router::add('/', $pageNamespace.'ProfilePage@index', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/profile/update', $pageNamespace.'ProfilePage@update', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/profile/logout', $pageNamespace.'ProfilePage@logout', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/profile/contacts', $pageNamespace.'ProfilePage@contacts', [
    $middlewareNamespace.'AuthMiddleware',
]);

Router::add('/register/get', $pageNamespace.'RegisterPage@get', [
    $middlewareNamespace.'GuestMiddleware',
]);
Router::add('/register/post', $pageNamespace.'RegisterPage@post', [
    $middlewareNamespace.'GuestMiddleware',
]);
Router::add('/register/check-unique-username', $pageNamespace.'RegisterPage@chechUniqueUsername', [
    $middlewareNamespace.'GuestMiddleware',
]);
Router::add('/register/check-unique-email', $pageNamespace.'RegisterPage@checkUniqueEmail', [
    $middlewareNamespace.'GuestMiddleware',
]);

Router::add('/login/get', $pageNamespace.'LoginPage@get', [
    $middlewareNamespace.'GuestMiddleware',
]);
Router::add('/login/post', $pageNamespace.'LoginPage@post', [
    $middlewareNamespace.'GuestMiddleware',
]);

Router::add('/contacts/create', $pageNamespace.'ContactsPage@create', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/contacts/store', $pageNamespace.'ContactsPage@store', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/contacts/edit/{id}', $pageNamespace.'ContactsPage@edit', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/contacts/update/{id}', $pageNamespace.'ContactsPage@update', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/contacts/delete/{id}', $pageNamespace.'ContactsPage@delete', [
    $middlewareNamespace.'AuthMiddleware',
]);

Router::add('/contacts-items/create/{contact_id}', $pageNamespace.'ContactsItemsPage@create', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/contacts-items/store', $pageNamespace.'ContactsItemsPage@store', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/contacts-items/edit/{id}', $pageNamespace.'ContactsItemsPage@edit', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/contacts-items/update/{id}', $pageNamespace.'ContactsItemsPage@update', [
    $middlewareNamespace.'AuthMiddleware',
]);
Router::add('/contacts-items/delete/{id}', $pageNamespace.'ContactsItemsPage@delete', [
    $middlewareNamespace.'AuthMiddleware',
]);