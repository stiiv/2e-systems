<?php 

namespace App\Helpers\Html;

class Form 
{
	public function select(string $name, array $options, $value = null, string $attributes = '') : string
	{
		$html  = sprintf('<select name="%s" id="%s" %s>', $name, $name, $attributes);
		foreach ($options as $key => $text) {
			$selected = ($value == $key) ? 'selected' : '';
			$html .= sprintf('<option %s value="%s">%s</option>', $selected, $key, $text);
		}
		$html .= '</select>';

		return $html;
	}
}