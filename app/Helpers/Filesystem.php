<?php 

namespace App\Helpers;

class Filesystem
{
	public static function delete_dir($target)
	{
		if(is_dir($target)){
	        $files = glob( $target.'*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

	        foreach( $files as $file ){
	            self::delete_dir( $file );      
	        }

	        if( is_dir($target) ) rmdir( $target );
	    } elseif(is_file($target)) {
	        unlink( $target );  
	    }
	}
}