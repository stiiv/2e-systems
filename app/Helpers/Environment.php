<?php 

namespace App\Helpers;

class Environment
{
	public function appName()
	{
		return getenv('APP_NAME');
	}

	public function devMode()
	{
		return getenv('APP_ENV') === 'dev';
	}

	public function currYear()
	{
		return date('Y.');
	}
}