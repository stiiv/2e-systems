<?php

namespace App\Repositories;

use App\Hash\BcryptHash;
use App\Models\User;
use App\Models\Gender;
use App\Models\State;
use App\Auth\User as AuthUser;
use App\Jobs\User\SendRegisterActivationEmail;
use Carbon\Carbon;
use DB;

class UserRepository extends Repository 
{
	public function model()
    {
        return User::class;
    }

    public function dropdownOptions(string $name)
    {
    	switch ($name) {
    		case 'gender':
    			return $this->app->make(Gender::class)->dropdownOptions();
    			break;
    		case 'state':
    			return $this->app->make(State::class)->dropdownOptions();
    			break;
    	}
    }

    public function create(array $data)
    {
        $data['password'] = (new BcryptHash)->make($data['password']);
        $data['register_token'] = $this->generateRegisterToken();
        $data['register_token_expiry'] = $this->registerTokenExpiry();

        $user = $this->model->create($data);
        (new SendRegisterActivationEmail($user))->run();
        return $user;
    }

    public function login(array $data) 
    {
        if( !isset($data['username'], $data['password']) )
            return;

        $user = $this->model
            ->where(function($query) use($data) {
                $query->where('username', '=', $data['username'])
                      ->orWhere('email', '=', $data['username']);
            });

        if( isset($data['_register_token']) && $data['_register_token'] !== '' ) {
            $register_token = $data['_register_token'];
            $user->where(function($query) use ($register_token) {
                $query->where('register_token', $register_token);
                $query->whereRaw("NOW() <= register_token_expiry");
            });
            // !!cron job to remove expired register_tokens!!
        } else {
            // it has to be active user
            $user->active();
        }

        $user = $user->first();

        if(!$user)
            return;

        $hash = new BcryptHash();

        if( !$hash->check($data['password'], $user->password) ) {
            $user->login_attemps = (int) $user->login_attemps++;
            $user->save();
            return;
        }

        if( !$user->active ) {
            $user->update([
                'active' => 1,
                'activated_at' => Carbon::now()
            ]);
            $user = $user->fresh();
        }

        AuthUser::login((int) $user->id);
        return AuthUser::get();
    }

    public function isUnique(string $column, $value, int $user_id = 0) : bool
    {
        $record = $this->model->where($column, '=', $value);

        if($user_id > 0) {
            $record->where('id', '!=', $user_id);
        }
        return $record->first() === null;
    }

    public function logout()
    {
        return AuthUser::logout();
    }

    public function generateRegisterToken(int $length = 32) : string
    {
        return base64_encode(random_bytes($length));
    }

    public function registerTokenExpiry(int $minutes = 60) : string
    {
        return Carbon::now()->addMinutes($minutes);
    }
}