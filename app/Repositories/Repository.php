<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Exceptions\RepositoryException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

abstract class Repository implements RepositoryInterface {
 
    /**
     * @var App
     */
    protected $app;
 
    /**
     * @var model
     */
    protected $model;

    /**
     * eager load relationships
     * @var array
     */
    protected $with = [];
 
    /**
     * @param App $app
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function __construct(App $app) 
    {
        $this->app = $app;
        $this->makeModel();
    }
 
    /**
     * Specify Model class name
     * 
     * @return mixed
     */
    abstract public function model();
 
    /**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel() 
    {
        $model = $this->app->make($this->model());
 
        if (!$model instanceof Model)
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
 
        return $this->model = $model;
    }

    /**
     * set globa relationships for model/repository
     * @param array $data relationships
     * @return $this
     */
    public function setRelations(array $data)
    {
        $this->with = $data;
        return $this;
    }

    /**
     * unset globa relationships for model/repository
     * @return $this
     */
    public function unssetRelations()
    {
        $this->with = [];
        return $this;
    }

    public function all($columns = ['*']) 
    {
        return $this->model->get($columns);
    }
 
    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = ['*']) 
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * empty model for Form::collective modul
     * @return empty Illuminate\Database\\Eloquent\Model
     */
    public function newModel($params = [])
    {
        return $this->model;
    }
 
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) 
    {
        return $this->model->create($data);
    }
 
    /**
     * @param $id
     * @param array $data
     * @param array $relationships
     * @param string $attribute
     * @return mixed
     */
    public function update($id, array $data, $relationships = [], $attribute="id") 
    {
        $model = $this->model->findOrFail($id);
        $model->update($data);

        if( count($relationships) > 0 ) {
            return $model->fresh($relationships);
        }

        if( count($this->with) > 0 ) {
            return $model->fresh($this->with);
        }
        return $model->fresh();
    }
 
    /**
     * @param $id
     * @return mixed
     */
    public function delete($id) 
    {
        $model = $this->model->findOrFail($id);
        $model->destroy($id);
        return $model;
    }
 
    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*']) 
    {
        return $this->model->findOrFail($id, $columns);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function findWithRelations($id, $relationships = [], $columns = ['*']) 
    {
        if( count($relationships) > 0 ) {
            return $this->model->with($relationships)->findOrFail($id, $columns);
        }

        if( count($this->with) > 0 ) {
            return $this->model->with($this->with)->findOrFail($id, $columns);
        }

        return $this->model->findOrFail($id, $columns);
    }

    /**
     * find first record or fail
     * @param  array  $columns [description]
     * @return [type]          [description]
     */
    public function findFirst($columns = ['*'])
    {
        return $this->model->firstOrFail($columns);
    }
 
    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = ['*']) 
    {
        return $this->model->where($attribute, '=', $value)->firstOrFail($columns);
    }
}