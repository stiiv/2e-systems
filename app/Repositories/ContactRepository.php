<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Auth\User as AuthUser;
use Carbon\Carbon;
use DB;

class ContactRepository extends Repository 
{
	public function model()
    {
        return Contact::class;
    }

    public function create(array $data)
    {
        $data['user_id'] = (int) AuthUser::get()->id;
        return $this->model->create($data);
    }

    public function delete($id) 
    {
        $model = $this->model->findOrFail($id);
        $model->items()->delete();
        $model->destroy($id);
        return $model;
    }
}