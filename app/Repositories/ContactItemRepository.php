<?php

namespace App\Repositories;

use App\Models\ContactItem;
use App\Auth\User as AuthUser;
use Carbon\Carbon;
use DB;

class ContactItemRepository extends Repository 
{
	public function model()
    {
        return ContactItem::class;
    }
}