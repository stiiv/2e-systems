<?php 

namespace App\Pages;

use System\Pages\Page;
use App\Repositories\UserRepository;
use App\Pages\Validators\LoginPageValidator as Validator;

class LoginPage extends Page
{
	protected $title = 'Login page';

	protected function repository()
	{
		return UserRepository::class;
	}

	public function get() 
	{
		$register_token = $this->request->get('register_token', '');
		return $this->loadView('login/get.html', [
			'title' => $this->title,
			'register_token' => $register_token,
		]);
	}

	public function post()
	{
		$this->httpMethodAllowed('POST');
		$validator = Validator::create($this->request);

		if( $validator->hasErrors() ) {
			return $this->redirectTo('/login/get');
		}

		if( $user = $this->repository->login($this->request->request->all()) ) {
			$this->flashSuccess('Loged in successfully!');
			return $this->redirectTo('/');
		}
		
		$this->flashError('Unable to login. Please, check your credentials!');
		return $this->redirectTo('/');
	}
}