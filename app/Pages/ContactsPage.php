<?php 

namespace App\Pages;

use System\Pages\Page;
use App\Repositories\ContactRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Pages\Validators\ContactsPageValidator as Validator;

class ContactsPage extends Page
{
	protected $title = 'Contact ';

	protected $response = [
		'errors' => [],
		'data' => []
	];

	protected function repository()
	{
		return ContactRepository::class;
	}

	public function create()
	{
		return $this->loadView('contacts/create.html', [
			'data' => $this->repository->newModel(),
		]);
	}

	public function store()
	{
		$this->httpMethodAllowed('POST');
		$validator = Validator::create($this->request);

		if( $validator->hasErrors() ) {
			$this->response['errors'] = $validator->getErrors();
		} else {
			$this->response['data'] = $this->repository->create( 
				$this->request->request->all() 
			);
		}
		return new JsonResponse($this->response);
	}

	public function edit($id)
	{
		$data = $this->repository->find( (int) $id );
		return $this->loadView('contacts/edit.html', [
			'data' => $data,
		]);
	}

	public function update($id)
	{
		$this->httpMethodAllowed('POST');
		$data = $this->repository->find( (int) $id );
		$validator = Validator::create($this->request);

		if( $validator->hasErrors() ) {
			$this->response['errors'] = $validator->getErrors();
		} else {
			$this->response['data'] = $this->repository->update( 
				$data->id, $this->request->request->all() 
			);
		}
		return new JsonResponse($this->response);
	}

	public function delete($id)
	{
		$this->httpMethodAllowed('POST');
		$this->response['data'] = $this->repository->delete((int) $id);
		return new JsonResponse( $this->response );
	}
}