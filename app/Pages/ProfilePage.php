<?php 

namespace App\Pages;

use System\Pages\Page;
use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Pages\Validators\ProfilePageValidator as Validator;

class ProfilePage extends Page
{
	protected $title = 'Welcome ';

	protected function repository()
	{
		return UserRepository::class;
	}

	public function index() 
	{
		$user = $this->currUser;
		$title = $this->title.$user->fullName();
		return $this->loadView('profile/index.html', [
			'title' => $title,
			'genderOptions' => $this->repository->dropdownOptions('gender'),
			'stateOptions' => $this->repository->dropdownOptions('state'),
			'data' => $user,
		]);
	}

	public function update()
	{
		$this->httpMethodAllowed('POST');
		$validator = Validator::create($this->request);

		if( $validator->hasErrors() ) {
			return $this->redirectTo('/');
		}

		if( $this->repository->update($this->currUser->id, $this->request->request->all()) ) {
			$this->flashSuccess('Updated successfully!');
			return $this->redirectTo('/');
		}

		$this->flashError('Error updating data!');
		return $this->redirectTo('/');
	}

	public function logout()
	{
		$this->repository->logout();
		return $this->redirectTo('/login/get');
	}

	public function contacts()
	{
		$contacts = $this->currUser->contacts()->with('items')->get();
		return new JsonResponse($contacts);
	}
}