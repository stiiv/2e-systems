<?php 

namespace App\Pages;

use System\Pages\Page;
use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Pages\Validators\RegisterPageValidator as Validator;

class RegisterPage extends Page
{
	protected $title = 'Register page';

	protected function repository()
	{
		return UserRepository::class;
	}

	public function get() 
	{
		return $this->loadView('register/get.html', [
			'title' => $this->title,
			'genderOptions' => $this->repository->dropdownOptions('gender'),
			'stateOptions' => $this->repository->dropdownOptions('state'),
		]);
	}

	public function post()
	{
		$this->httpMethodAllowed('POST');
		$validator = Validator::create($this->request);

		if( $validator->hasErrors() ) {
			return $this->redirectTo('/register/get');
		}

		if( $this->repository->create($this->request->request->all()) ) {
			$this->flashSuccess('Success! Please check your email for activation link!');
			return $this->redirectTo('/login/get');
		}

		$this->flashError('Error updating data!');
		return $this->redirectTo('/register/get');
	}

	public function chechUniqueUsername()
	{
		$this->httpMethodAllowed('POST');
		if( !$this->request->isXmlHttpRequest() )
			return;

		$unique = $this->repository->isUnique('username', $this->request->get('username'));
		return new JsonResponse($unique);
	}

	public function checkUniqueEmail()
	{
		$this->httpMethodAllowed('POST');
		if( !$this->request->isXmlHttpRequest() )
			return;

		$unique = $this->repository->isUnique('email', $this->request->get('email'));
		return new JsonResponse($unique);
	}
}