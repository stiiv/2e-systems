<?php 

namespace App\Pages\Validators;

use Symfony\Component\HttpFoundation\Request;

abstract class JsonValidator extends Validator
{	
	public function __construct(Request $request)
	{
		$this->request = $request;
		$this->data = $this->request->request->all();
		$this->setValidator();
	}
}