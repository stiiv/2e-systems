<?php 

namespace App\Pages\Validators;


class ProfilePageValidator extends Validator
{
	protected function getRules() : array 
	{
		return [
			'email' => 'required|valid_email'
		];
	}
}