<?php 

namespace App\Pages\Validators;


class ContactsPageValidator extends JsonValidator
{
	protected function getRules() : array 
	{
		return [
			'name' => 'required'
		];
	}
}