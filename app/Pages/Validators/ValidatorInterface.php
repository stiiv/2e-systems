<?php 

namespace App\Pages\Validators;

interface ValidatorInterface
{
	public function hasErrors() : bool;

	public function getErrors() : array;
}