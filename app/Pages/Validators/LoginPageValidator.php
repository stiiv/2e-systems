<?php 

namespace App\Pages\Validators;


class LoginPageValidator extends Validator
{
	protected function getRules() : array 
	{
		return [
			'username' => 'required',
			'password' => 'required',
		];
	}
}