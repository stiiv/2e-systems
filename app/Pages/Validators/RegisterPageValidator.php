<?php 

namespace App\Pages\Validators;

use Illuminate\Container\Container as App;
use App\Repositories\UserRepository;

class RegisterPageValidator extends Validator 
{
	protected function setValidator()
	{
		$app = new App;
		$user = $app->make(UserRepository::class);
		\GUMP::add_validator('is_unique', function($field, $input, $param = NULL) use ($user) {
		    return $user->isUnique($field, $input);
		}, 'Field {field} already exists');
		$valid = \GUMP::is_valid($this->data, $this->getRules());

		if( is_array($valid) ) {
			$this->errors = $valid;
		}
	}

	protected function getRules() : array 
	{
		return [
			'username' => 'required|is_unique',
			'email' => 'required|valid_email|is_unique',
			'password' => 'required|min_len,6',
		];
	}
}