<?php 

namespace App\Pages\Validators;


class ContactsItemsPageValidator extends JsonValidator
{
	protected function getRules() : array 
	{
		return [
			'label' => 'required',
			'item' => 'required',
		];
	}
}