<?php 

namespace App\Pages\Validators;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

abstract class Validator implements ValidatorInterface
{
	protected $request;

	protected $session;

	protected $data = [];

	protected $errors = [];
	
	public function __construct(Request $request)
	{
		$this->request = $request;
		$this->data = $this->request->request->all();
		$this->setValidator();

		$this->setSession();
		$this->setSessionData();
	}

	public static function create(Request $request) 
	{
		return new static($request);
	}

	public function hasErrors() : bool
	{
		return (bool) count($this->errors);
	}

	public function getErrors() : array
	{
		return $this->errors;
	}

	protected function setValidator()
	{
		$validator = new \GUMP();
		$this->data = $validator->sanitize($this->data);
		$validator->validation_rules($this->getRules());
		$validator->filter_rules($this->getFilterRules());

		$validated_data = $validator->run($this->data);

		if($validated_data === false) {
			$this->errors = $validator->get_readable_errors();
		}
	}

	public function getFilterRules() : array
	{
		$fields = array_keys($this->data);
		return array_fill_keys($fields, 'trim|sanitize_string');
	}

	abstract protected function getRules() : array;

	protected function setSession()
	{
		if( $this->request->hasSession() ) 
			$this->session = $this->request->getSession();
		else 
			$this->session = new Session();
	}

	protected function setSessionData()
	{
		if( !$this->hasErrors() ) {
			return $this->removeSessionData();
		}

		foreach ($this->data as $key => $value) {
			$this->session->set($key, $value);
		}

		$this->session->set('errors', $this->getErrors());

		$this->request->setSession($this->session);
	}

	protected function removeSessionData()
	{
		foreach ($this->data as $key => $value) {
			if( $this->session->has($key) )
				$this->session->remove($key, $value);
		}

		if( $this->session->has('errors') )
			$this->session->remove('errors');
	}
}