<?php 

namespace App\Pages;

use System\Pages\Page;
use App\Repositories\ContactItemRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Pages\Validators\ContactsItemsPageValidator as Validator;

class ContactsItemsPage extends Page
{
	protected $title = 'Contacts Items Page';

	protected $response = [
		'errors' => [],
		'data' => []
	];

	protected function repository()
	{
		return ContactItemRepository::class;
	}

	public function create($contact_id)
	{
		$data = $this->repository->newModel();
		$data->contact_id = (int) $contact_id;
		return $this->loadView('contacts-items/create.html', [
			'data' => $data,
		]);
	}

	public function store()
	{
		$this->httpMethodAllowed('POST');
		$validator = Validator::create($this->request);

		if( $validator->hasErrors() ) {
			$this->response['errors'] = $validator->getErrors();
		} else {
			$this->response['data'] = $this->repository->create( 
				$this->request->request->all() 
			);
		}
		return new JsonResponse($this->response);
	}

	public function edit($id)
	{
		$data = $this->repository->find( (int) $id );
		return $this->loadView('contacts-items/edit.html', [
			'data' => $data,
		]);
	}

	public function update($id)
	{
		$this->httpMethodAllowed('POST');
		$data = $this->repository->find( (int) $id );
		$validator = Validator::create($this->request);

		if( $validator->hasErrors() ) {
			$this->response['errors'] = $validator->getErrors();
		} else {
			$this->response['data'] = $this->repository->update( 
				$data->id, $this->request->request->all() 
			);
		}
		return new JsonResponse($this->response);
	}

	public function delete($id)
	{
		$this->httpMethodAllowed('POST');
		$this->response['data'] = $this->repository->find( (int) $id );
		$this->repository->delete($id);
		return new JsonResponse( $this->response );
	}
}