<?php 

namespace App\Models;

interface DropdownModelInterface
{
	public function dropdownOptions($params = []) : array;
}