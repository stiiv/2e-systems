<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class User extends Model 
{
	protected $table = 'users';

	protected $primaryKey = 'id';

	protected $guarded = ['id', 'password_match'];
	

	public function fullName()
	{
		return $this->first_name.' '.$this->last_name;
	}

	public function contacts()
	{
		return $this->hasMany(Contact::class)->orderBy('name');
	}

	public function gender()
	{
		return $this->belongsTo(Gender::class);
	}

	public function state()
	{
		return $this->belongsTo(State::class);
	}

	public function scopeActive($query)
	{
		return $query->where('active', 1);
	}

	// mutators
	public function setDobAttribute($value)
	{
		if($value)
			$this->attributes['dob'] = Carbon::createFromFormat(DATE_FORMAT_DISPLAY, $value)->format(DATE_FORMAT_SAVE);
		else 
			$this->attributes['dob'] = '';
	}

	public function getDobAttribute($value)
	{
		if($value)
			return Carbon::createFromFormat(DATE_FORMAT_SAVE, $value)->format(DATE_FORMAT_DISPLAY);

		return '';
	}
}