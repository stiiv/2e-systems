<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ContactItem extends Model 
{
	protected $table = 'contact_items';

	protected $primaryKey = 'id';

	protected $guarded = ['id'];

	public function contact()
	{
		return $this->belongsTo(Contact::class);
	}
}