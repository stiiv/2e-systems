<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model 
{
	protected $table = 'contacts';

	protected $primaryKey = 'id';

	protected $fillable = ['user_id', 'name'];

	public function items()
	{
		return $this->hasMany(ContactItem::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}