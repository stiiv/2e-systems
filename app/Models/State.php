<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class State extends Model implements DropdownModelInterface
{
	protected $table = 'states';

	protected $primaryKey = 'id';

	public $timestamps = false;

	public function dropdownOptions($params = []) : array
	{
		return $this->pluck('name', 'id')->toArray();
	}

	public function users()
	{
		return $this->hasMany(User::class);
	}
}