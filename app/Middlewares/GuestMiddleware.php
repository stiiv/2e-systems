<?php

namespace App\Middlewares;
use Symfony\Component\HttpFoundation\Request;
use System\Middlewares\MiddlewareInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use App\Auth\User;

class GuestMiddleware implements MiddlewareInterface
{	
	public function canProceed(Request $request)
	{
		$user = User::get();

		if($user) {
			$response = new RedirectResponse('/');
			return $response->send();
		}
	}
}