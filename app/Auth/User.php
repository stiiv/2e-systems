<?php 

namespace App\Auth;

use App\Models\User as UserModel;
use Symfony\Component\HttpFoundation\Session\Session;

class User
{
	protected static $user;

	protected static $session;

	public static $sessionKeyName = 'user_id';

	public static function get()
	{
		self::sessionInit();
		if( self::$session->has(self::$sessionKeyName) ) {
			self::$user = UserModel::active()->find( (int) self::$session->get(self::$sessionKeyName) );

			if(null === self::$user) {
				self::$session->clear();
			}
		}

		return self::$user;
	}

	public static function login(int $user_id)
	{
		self::sessionInit();
		self::$session->set(self::$sessionKeyName, $user_id);
	}

	public static function logout()
	{
		self::sessionInit();
		self::$session->clear();
	}

	protected static function sessionInit()
	{
		if( null === self::$session ) {
			self::$session = new Session;
		}
	} 
}