<?php

namespace App\Jobs;

interface JobInterface
{
	public function run();
}