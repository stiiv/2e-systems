<?php 

namespace App\Jobs\User;

use App\Models\User as UserModel;
use App\Jobs\JobInterface;

class SendRegisterActivationEmail implements JobInterface
{
	protected $user;

	public function __construct(UserModel $user)
	{	
		$this->user = $user;
	}

	public function run()
	{
		// send email to $this->user->email 
		// with link /login/get?register_token=$this->user->register_token
	}
}