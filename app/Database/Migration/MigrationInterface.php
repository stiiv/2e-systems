<?php 

namespace App\Database\Migration;

interface MigrationInterface
{
	/**
	 * build up database schema
	 * @return void
	 */
	public function up();

	/**
	 * build down database schema
	 * @return void
	 */
	public function down();
}