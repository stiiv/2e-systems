<?php 

namespace App\Database\Migration;

use Illuminate\Database\Capsule\Manager as DB;

class Migrate implements MigrationInterface
{

	public static function run()
	{
		return new static;
	}

	/**
	 * build up database schema
	 * @return void
	 */
	public function up()
	{
		DB::schema()->defaultStringLength(191);
		
		DB::schema()->create('users', function ($table) {
		    $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password', 100);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('dob')->nullable();
            $table->integer('gender_id')->unsigned()->index();
            $table->integer('state_id')->unsigned()->index();
            $table->string('phone', 30)->nullable();
            $table->boolean('active')->default(0);
            $table->timestamp('activated_at')->nullable();
            $table->integer('login_attemps')->unsigned()->default(0);
            $table->string('register_token', 100)->nullable();
            $table->timestamp('register_token_expiry')->nullable();
            $table->timestamp('registered_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
		});

		DB::schema()->create('password_resets', function ($table) {
		    $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
		});

		DB::schema()->create('genders', function ($table) {
		    $table->increments('id');
            $table->string('name');
		});

		DB::schema()->create('states', function ($table) {
		    $table->increments('id');
            $table->string('code')->nullable();
            $table->string('name')->nullable();
		});

		DB::schema()->create('contacts', function ($table) {
		    $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('name');
            $table->timestamps();
		});

		DB::schema()->create('contact_items', function ($table) {
		    $table->increments('id');
            $table->integer('contact_id')->unsigned()->index();
            $table->string('item');
            $table->string('label');
            $table->timestamps();
		});
	}

	/**
	 * build down database schema
	 * @return void
	 */
	public function down()
	{
		DB::schema()->drop('users');
		DB::schema()->drop('genders');
		DB::schema()->drop('password_resets');
		DB::schema()->drop('states');
		DB::schema()->drop('contacts');
		DB::schema()->drop('contact_items');
	}
}