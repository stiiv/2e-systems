<?php 

namespace App\Database\Seeders;

use App\Hash\BcryptHash;

use App\Models\User;
use App\Models\Gender;
use App\Models\State;

class Seeder
{
	public static function seed()
	{
		Gender::unguard();
		Gender::create([
			'id' => 1,
			'name' => 'Male'
		]);
		Gender::create([
			'id' => 2,
			'name' => 'Female'
		]);
		Gender::reguard();

		State::unguard();
		State::create([
			'id' => 1,
			'code' => '+386',
			'name' => 'Croatia',
		]);
		State::create([
			'id' => 2,
			'code' => '+49',
			'name' => 'Germany',
		]);
		State::reguard();

		User::unguard();
		User::create([
			'username' => 'stiiv',
			'email' => 'ivan.stipic6@gmail.com',
			'password' => (new BcryptHash)->make('secret'),
			'first_name' => 'Ivan',
			'last_name' => 'Stipić',
			'gender_id' => 1,
			'state_id' => 1
		]);
		User::reguard();
	}
}