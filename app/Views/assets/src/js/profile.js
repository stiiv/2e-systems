import validate from 'jquery-validation';
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css';
import datepicker from 'bootstrap-datepicker';
import * as settings from './settings.js'
settings.validator();

let profile = {};
profile.$form = $("#profile-form");
profile.$username = $('#username');
profile.$first_name = $('#first_name'),
profile.$last_name = $('#last_name'),
profile.$email = $('#email');
profile.$dob = $('#dob');

let requiredInput = "Required field";
let minlengthInput = "Field must have min of {0} characters";

profile.$dob.datepicker(settings.datepicker);

profile.$form.validate({
    rules : {
        username: {
			required: true,
			minlength: 4
		},
        email: {
			required: true,
			email: true
		}
    },
    messages : {
        username : {
            required : requiredInput,
			minlength : minlengthInput
        },
        email : {
            required : requiredInput,
			email : "E-mail <em>nije</em> ispravnog formata"
        }
    }
});