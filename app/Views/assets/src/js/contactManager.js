export default class ContactManager {

	constructor($rootTemplate) { 
		this.data = [];
		this.dataUrl = '/profile/contacts';
		this.$rootTemplate = $rootTemplate;
		this.$modal = null;
		this.html = '';
		this.lastContactSaved = null;
   }

   render() {
   		this.data = [];
   		this.html = '';
   		this.$rootTemplate.html(this.html);

   		this.getData().done((data) => {
   			if(!data.length)
   				return;

   			for(let contact of data) {
   				this.add(contact);
   			}
   			this.$rootTemplate.html(this.html);
   			this.$rootTemplate.trigger('contacts:rendered');
   		});		
   }

   getData() {
   		return $.get(this.dataUrl);
   }

   add(contact) {
   		this.data.push(contact);
   		this.makeTemplate(contact);
   }

   makeTemplate(contact) {
   		let items = `
   			<button  
   				data-create-contact-item-url="/contacts-items/create/${contact.id}" 
   				class="btn btn-secondary add-contact-item-btn">
   				Add Contact Item
   			</button>
   			<br>
   		`;
   		if(contact.items.length) {
   			items += `<ul class="list-group clickable">`;
   			for(let item of contact.items) {
   				items += `
					<li data-edit-contact-item-url="/contacts-items/edit/${item.id}" class="edit-contact-item list-group-item">
						<h4>${item.label}</h4>
        				<p>${item.item}</p>
					</li>
   				`;
   			}
   			items += `</ul>`;
   		}

   		const isOpened = parseInt(this.lastContactSaved) === parseInt(contact.id) ? 'in' : '';

   		const html = `
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title" data-contacts-id="${contact.id}">
						<a 
							class="accordion-toggle" 
							data-toggle="collapse" 
							data-parent="#contacts-accordion" 
							href="#collapse-${contact.id}">
						  ${contact.name} 
						</a>
						<a 
							href="JavaScript:void(0);" 
							data-edit-contact-url="/contacts/edit/${contact.id}" 
							class="edit-contact btn btn-default pull-right">
							Edit / Delete
						</a>
					</h4>
				</div>
				<div id="collapse-${contact.id}" class="panel-collapse collapse ${isOpened}">
					<div class="panel-body">
						${items}
					</div>
				</div>
			</div>
   		`;
   		this.html += html;
   }

   loadByUrl(url, beforeRenderCallback) {
   		const $modal = this.$modal;
   		const $errorBox = $modal.find('.alert-danger');
   		let _self = this;

   		$.get(url).done((data) => {
   			$modal.find('.modal-body').html(data);
   			$modal.modal('show');

   			let $form = $modal.find('form');
   			$form.on('submit', function(e) {
   				e.preventDefault();
   				const url = $(this).attr('action');

               if( $(this).hasClass('delete-form') ) {
                  if( !confirm('Delete? Are you sure?') ) 
                     return;
               }

   				$.post(url, $(this).serializeArray()).done((response) => {
   					if( response.errors && response.errors.length ) {
   						let errors = '<hr>';
   						for(let error of response.errors) {
   							errors += `<p>* ${error}</p>`;
   						}
   						$errorBox.html(errors).show();
   						return;
   					}
   					$errorBox.html('').hide();
   					$modal.modal('hide');
   					if (typeof beforeRenderCallback === "function") {
   					    beforeRenderCallback(response.data);
   					}
   					_self.render();
   				});
   			});
   		});
   }
}