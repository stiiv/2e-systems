export function validator() {
  
    $.validator.setDefaults({
        //debug : true,
        errorClass : "help-block",
        highlight : function(element) {
            $(element).closest(".form-group").addClass("has-error").removeClass("valid");
            var $tabPane = $(element).closest("div.tab-pane"),
                $navTab = $("a[href='#"+$tabPane.attr("id")+"']");

            $navTab.addClass("has-error").removeClass("valid");

            // activate tab on error
            $(".tab-content").find("div.tab-pane:hidden:has(div.has-error)").each( function(){
                var id = $(this).attr("id");
                $('a[href="#'+id+'"]').tab('show');
            });
        },

        unhighlight : function(element) {
            // remove help-block for select
            $(element).closest(".form-group").find(".help-block").remove();

            $(element).closest(".form-group").removeClass("has-error").addClass("valid");
            var $tabPane = $(element).closest("div.tab-pane"),
                $navTab = $("a[href='#"+$tabPane.attr("id")+"']");

            $navTab.removeClass("has-error").addClass("valid");
        },

        onkeyup: function(element) {
            this.element(element);  // <- "eager validation"
        },
        onfocusout: function(element) {
            this.element(element);  // <- "eager validation"
        },
        onclick: function(element) {
            this.element(element);  // <- "eager validation"
        },

        onsubmit: true,

        submitHandler: function(form) {
            if( $(form).valid() )
                form.submit();
        },

        ignore: ""
    });
};

export const datepicker = {
    format: "dd/mm/yyyy",
    clearBtn: true
};