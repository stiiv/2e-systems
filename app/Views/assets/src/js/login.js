import validate from 'jquery-validation';
import * as settings from './settings.js'
settings.validator();

let login = {};
login.$form = $("#login-form");
login.$username = $('#username');
login.$password = $('#password');

let requiredInput = "Required field";

login.$form.validate({
    rules : {
        username: {
			required: true
		},
        password: {
			required: true,
		},
    },
    messages : {
        username : {
            required : requiredInput
        },
        password : {
            required : requiredInput
        }
    }
});