import ContactManager from './contactManager.js';
import { Modal } from 'bootstrap';

const cm = new ContactManager( $("#contacts-accordion") );
cm.$modal = $("#contactsModal");
cm.render();

const saveContact = function(data) {
	cm.lastContactSaved = data.id;
}

const saveContactItem = function(data) {
	cm.lastContactSaved = data.contact_id;
}

$('#add-contact-btn').on('click', function() {
	const url = $(this).attr('data-create-contact-url');
	cm.loadByUrl(url, saveContact);
});

cm.$rootTemplate.on('click', '.edit-contact', function() {
	const url = $(this).attr('data-edit-contact-url');
	cm.loadByUrl(url, saveContact);
});

cm.$rootTemplate.on('click', '.add-contact-item-btn', function() {
	const url = $(this).attr('data-create-contact-item-url');
	cm.loadByUrl(url, saveContactItem);
});

cm.$rootTemplate.on('click', '.edit-contact-item', function() {
	const url = $(this).attr('data-edit-contact-item-url');
	cm.loadByUrl(url, saveContactItem);
});