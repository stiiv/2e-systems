import validate from 'jquery-validation';
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css';
import datepicker from 'bootstrap-datepicker';
import * as settings from './settings.js'
settings.validator();

let register = {};
register.$form = $("#register-form");
register.$username = $('#username');
register.$first_name = $('#first_name'),
register.$last_name = $('#last_name'),
register.$email = $('#email');
register.$password = $('#password'),
register.$password_match = $('#password_match');
register.$dob = $('#dob');

let requiredInput = "Required field";
let minlengthInput = "Field must have min of {0} characters";
let equalToInput = "Value must match field 'Password'";
let remoteInput = "Username already exists";

register.$dob.datepicker(settings.datepicker);

register.$form.validate({
    rules : {
        username: {
			required: true,
			minlength: 4,
			remote: {
				param: {
					type: "POST",
					dataType: "json",
					url: "/register/check-unique-username",
					data: {
						username: function() {
							return register.$username.val();
						}
					}
				}
			}
		},
        email: {
			required: true,
			email: true,
			remote: {
				param: {
					type: "POST",
					dataType: "json",
					url: "/register/check-unique-email",
					data: {
						username: function() {
							return register.$email.val();
						}
					}
				}
			}
		},
        password: {
			required: true,
			minlength: 6,
		},
		password_match: {
			required: true,
			minlength: 6,
			equalTo: "#password"
		}
    },
    messages : {
        username : {
            required : requiredInput,
			minlength : minlengthInput,
			remote : remoteInput
        },
        email : {
            required : requiredInput,
			email : "E-mail <em>nije</em> ispravnog formata",
			remote : "Email already taken"
        },
        password : {
            required : requiredInput,
			minlength : minlengthInput
        },
        password_match : {
            required : requiredInput,
			minlength : minlengthInput,
			equalTo: equalToInput
        }
    }
});