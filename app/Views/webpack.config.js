var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  "resolve": {
  	"modules": [
  	  __dirname+"/node_modules"
  	]
  },
  "entry": {
    app: "./assets/src/js/main.js",
    register: "./assets/src/js/register.js",
    login: "./assets/src/js/login.js",
    profile: "./assets/src/js/profile.js",
    contacts: "./assets/src/js/contacts.js",
  },
  "output": {
    "path": __dirname+"/assets/dist/js",
    "filename": "[name].bundle.js"
  },
  "watch": true,
  "module": {
  	"rules": [
    	{
    	  "test": /\.css$/,
    	  "use": [
    	    { "loader": "style-loader" },
    	    { "loader": "css-loader" }
    	  ]
    	},{
    	  "test": /\.js$/,
    	  "exclude": /(node_modules|bower_components)/,
    	  "use": { 
    	    "loader": "babel-loader",
    	    "options": {
    	      "presets": ["@babel/preset-env"]
    	    }
    	  } 
    	},{
        "test": /\.(png|jpg|gif|svg|ttf|woff|woff2|eot)$/i,
        "use": [
          {
            "loader": "url-loader",
            "options": {
              "limit": 8192
            }
          }
    	  ]
      },{
        "test": /\.(png|jpg|gif|svg|ttf|woff|woff2|eot)$/i,
        "use": [
          {
            "loader": "file-loader",
            "options": {}
          }
        ]
      }
    ]
  }, "plugins": [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new ExtractTextPlugin("styles.css")
  ]
};