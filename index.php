<?php

define('BASE_DIR', __DIR__);
define('TIMEZONE', 'Europe/Zagreb');
define('DATE_FORMAT_SAVE', 'Y-m-d');
define('DATE_FORMAT_DISPLAY', 'd/m/Y');

date_default_timezone_set(TIMEZONE);

require_once BASE_DIR.'/vendor/autoload.php';

use System\Router\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Dotenv\Dotenv;

use Illuminate\Database\Capsule\Manager as DB;


$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

// database config
$capsule = new DB;
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => getenv('DB_HOST'),
    'database'  => getenv('DB_NAME'),
    'username'  => getenv('DB_USER'),
    'password'  => getenv('DB_PASSWORD'),
    'charset'   => 'utf8mb4',
    'collation' => 'utf8mb4_general_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();
//-> end database config

/**
 * Uncoment line below for database migrations ( remember to set database parameters in .env file first!! )
 */
// \App\Database\Migration\Migrate::run()->up(); dd('Database migration!');
// 
/**
 * Uncoment line below for seeding database 
 */
// \App\Database\Seeders\Seeder::seed(); dd('Database seeded!');

$request = Request::createFromGlobals();
Router::setRequest($request);
include_once BASE_DIR.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'routes.php';

try {
    $content = Router::resolve( $request->getPathInfo() );
    if( $content instanceof JsonResponse ) {
        return $content->send();
    }
    $response = new Response($content);
    return $response->send();
} catch(\System\Router\RouteNotExistsExcepion $e) {
    // attempt to load static assets file first
    $viewDir = implode(DIRECTORY_SEPARATOR, [BASE_DIR, 'app', 'Views']);
    $segments = explode('/', $request->getPathInfo());
    $file = $viewDir.implode(DIRECTORY_SEPARATOR, $segments);

    if( file_exists($file) ) {
        $response = new BinaryFileResponse($file);
        return $response->send();
    } 

    return (new BinaryFileResponse(
        $viewDir.DIRECTORY_SEPARATOR.'errors'.DIRECTORY_SEPARATOR.'404.html'
    ))->send();
}
